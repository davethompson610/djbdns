# djbdns/dnscache Technical Exercise

The following are notes on the changes I made for each of the programming challenges.

## Programming Challenges:

### 1. myip.opendns.com

The changes to implement this are in the following commit: [0b0fc1a](https://bitbucket.org/davethompson610/djbdns/commits/0b0fc1a7d758593d28660ce12470d85d9112ab1e)

### 2. okclient()

Modified okclient.c to build a permit_list from the file names in the ip directory. This list is built during initialization and updated whenever the ip directory is modified. I used a thread to monitor the directory for modifications (10 second polling loop) and re-builds the list if the modification time has changed. This eliminates the need for stat calls during the query processing.

The changes to implement this are in the following commit: [72c8c18](https://bitbucket.org/davethompson610/djbdns/commits/72c8c180db8240581c173314ae5de5363124eb32)

### 3. cache.c

Added method `cache_clear()` which can be used to delete an entry from the cache.

The changes to implement this are in the following commit: [838e646](https://bitbucket.org/davethompson610/djbdns/commits/838e646fcef2390c0cb0a9e784b5fea31ed859a0)

### 4. A new cache

Added a new AVL tree based cache which is implemented in `cache_avl.c`. I believe an AVL tree is well suited for these sort of lookups and provides the implementation provides the following benefits over the existing cache mechanism:

1. Entries are stored in the AVL tree with the following sort order: TLD, Domain, Sub-domain, and Type. This allows for filter based searches of the tree and is used to implement the following methods: `cache_avl_treewalk()`, and `cache_avl_clear_with_filter()`. The later can be used to delete all cache entries for a given domain, e.g. `google.com`.
1. Cache entries are also included in two separate double linked lists, one sorted by age, and the other sorted by usage. These are used when selecting entries to be deleted in order to keep the cache in compliance. The age list is used to check for TTL expiration times (expired entries are always removed first). The usage list is then used to select additional entries to be deleted if the cache size limit is exceeded. Rather than just selecting the oldest entry, we select the entry that has been the least used.
1. Added some UT for this in `cache_avl_test.c`.

The changes to implement this are in the following commits: [ef49177](https://bitbucket.org/davethompson610/djbdns/commits/ef49177031454ee8d55fd8b3d12450b0be26c95b), [d84fd6d](https://bitbucket.org/davethompson610/djbdns/commits/d84fd6d173d65826963bd57f1dfce57b9f907878), [f7a8321](https://bitbucket.org/davethompson610/djbdns/commits/f7a8321ee39d55d3ae7028964ba54f189f636a66), [7642467](https://bitbucket.org/davethompson610/djbdns/commits/7642467808714925b27f8d94662cb0c731186fe4)
