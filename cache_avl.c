/*
 *  AVL tree based caching mechanism used for caching domain info.
 *  The AVL tree balancing logic was based on algorithm found here:
 *  https://www.geeksforgeeks.org/avl-tree-set-1-insertion/
 */

#include <stdlib.h>
#include <string.h>
#include "tai.h"
#include "cache_avl.h"

#define MAX(a, b) ((a > b) ? a : b)

// Double linked list node
struct dllnode {
  struct dllnode *next;
  struct dllnode *prev;
  int             on_list;
};

// Double linked list
struct dllroot {
  struct dllnode *head;
  struct dllnode *tail;
  int             node_count;
};

// AVL tree node
struct avlnode {
  struct cache_entry *left;
  struct cache_entry *right;
  int                 height;
};

// This defines a cache entry.
struct cache_entry {
  struct cache_entry_key key;
  char *                 data;
  unsigned int           data_len;
  unsigned int           entry_len;
  struct tai             expire;

  struct avlnode         avl_node;
  struct dllnode         age_node;
  struct dllnode         usage_node;

  // The key strings and data is packed here...
};

// Used to get the pointer to the containing instance from a dllnode pointer
#define container_of(ptr, type, member) \
    (type *)((char *)(ptr) - (char *) &((type *)0)->member)

static struct cache_entry *avl_root = NULL;
static struct dllroot age_root = {NULL, NULL, 0};
static struct dllroot usage_root = {NULL, NULL, 0};
static unsigned int cache_size_max = 0;
static unsigned int cache_size = 0;

// Add a node to the end of the list
void dll_push_back(struct dllroot *list, struct dllnode *node)
{
  if (list->tail) {
    list->tail->next = node;
  } else {
    list->head = node;
  }
  node->prev = list->tail;
  node->next = NULL;
  node->on_list = 1;
  list->tail = node;
  list->node_count++;
}

// Move a node already on the list to the end of the list
void dll_move_back(struct dllroot *list, struct dllnode *node)
{
  if (!node->on_list || list->tail == node) {
    return;
  }
  if (node->prev) {
    node->prev->next = node->next;
  } else {
    list->head = node->next;
  }
  node->next->prev = node->prev;
  list->tail->next = node;
  node->prev = list->tail;
  node->next = NULL;
  list->tail = node;
}

// Remove a node from the list
void dll_remove(struct dllroot *list, struct dllnode *node)
{
  if (!node->on_list) {
    return;
  }
  if (node->prev) {
    node->prev->next = node->next;
  } else {
    list->head = node->next;
  }
  if (node->next) {
    node->next->prev = node->prev;
  } else {
    list->tail = node->prev;
  }
  node->prev = NULL;
  node->next = NULL;
  node->on_list = 0;
  list->node_count--;
}

// Create a cache entry
struct cache_entry *create_cache_entry(const char type[2], const char *domain, const char *data, unsigned int datalen, uint32 ttl)
{
  static char nil = '\0';

  // Count the number of domain parts
  unsigned int dom_parts = 0;
  unsigned int dom_len = 1;
  const char *cp = domain;
  while (*cp) {
    dom_parts++;
    dom_len += *cp + 1;
    cp += (unsigned int) *cp + 1;
  }

  // Compute the size of the cache entry that will be needed
  unsigned int entry_len = sizeof(struct cache_entry)+dom_len+datalen;
  if (cache_size_max < entry_len) return NULL;

  // Create a new cache new_entry
  struct cache_entry *new_entry = malloc(entry_len);
  if (!new_entry) {
    return NULL;
  }
  memset(new_entry, 0, sizeof(struct cache_entry));
  new_entry->entry_len = entry_len;
  new_entry->key.tld = &nil;
  new_entry->key.domain = &nil;
  new_entry->key.subdomain = &nil;
  memcpy(new_entry->key.type, type, 2);
  if (ttl > 0) {
    struct tai now;
    tai_now(&now);
    tai_uint(&new_entry->expire, ttl);
    tai_add(&new_entry->expire, &new_entry->expire, &now);
  }
  char *post = (char *)&new_entry[sizeof(struct cache_entry)];

  // Setup the key in the new cache entry
  cp = domain;
  if (dom_parts > 2) {
    const char *subdom = cp;
    dom_len = 1;
    while (dom_parts > 2) {
      dom_parts--;
      dom_len += *cp + 1;
      cp += (unsigned int) *cp + 1;
    }
    memcpy(post, subdom, dom_len);
    post[dom_len-1] = '\0';
    new_entry->key.subdomain = post;
    post += dom_len;
  }
  if (dom_parts > 1) {
    dom_len = *cp;
    memcpy(post, cp + 1, dom_len);
    post[dom_len] = '\0';
    new_entry->key.domain = post;
    post += dom_len + 1;
    cp += dom_len + 1;
    dom_parts--;
  }
  if (dom_parts > 0) {
    dom_len = *cp;
    memcpy(post, cp + 1, dom_len);
    post[dom_len] = '\0';
    new_entry->key.tld = post;
    post += dom_len + 1;
  }

  // Copy the data into the new cache entry
  if (datalen > 0) {
    memcpy(post, data, datalen);
    new_entry->data = post;
    new_entry->data_len = datalen;
  }

  return new_entry;
}

// Delete a cache entry (assumes has already been removed from the AVL tree)
void delete_cache_entry(struct cache_entry *entry)
{
  if (entry->usage_node.on_list) {
    cache_size -= entry->entry_len;
  }
  dll_remove(&age_root, &entry->age_node);
  dll_remove(&usage_root, &entry->usage_node);
  free(entry);
}

// Get the height of the tree at the specified node
int avl_height(struct cache_entry *entry)
{
  if (!entry) {
    return 0;
  }
  return entry->avl_node.height;
}

// Get the balance factor of the specified node
int avl_balance(struct cache_entry *entry)
{
  if (!entry) {
    return 0;
  }
  return avl_height(entry->avl_node.left) - avl_height(entry->avl_node.right);
}

// Compare two strings. Returns -1 if s1 < s2, 0 if s1 == s2, 1 if s1 > 2
int cmp_strings(const char *str1, const char *str2, int max_len, int dom_format)
{
  int result = 0;
  const char *s1 = str1;
  const char *s2 = str2;
  int len1 = strnlen(s1, max_len);
  int len2 = strnlen(s2, max_len);

  // Skip first length byte if string is in the domain format
  if (dom_format) {
    if (len1 > 1) {
      s1 = &str1[1];
      len1--;
    }
    if (len2 > 1) {
      s2 = &str2[1];
      len2--;
    }
  }

  if (len1 == len2) {
    if (len1 > 0) {
      result = memcmp(s1, s2, len1);
    }
  } else if (len1 < len2) {
    if (len1 > 0) {
      result = memcmp(s1, s2, len1);
    }
    if (result == 0) {
      result = -1;
    }
  } else {
    if (len2 > 0) {
      result = memcmp(s1, s2, len2);
    }
    if (result == 0) {
      result = 1;
    }
  }
  return result;
}

// Compare two nodes. Return 0 if equal, -1 if s1 is less than s2, 1 if s1 is greater than s2
int avl_cmp_nodes(struct cache_entry *s1, struct cache_entry *s2)
{
  int result = cmp_strings(s1->key.tld, s2->key.tld, 256, 0);
  if (result == 0) {
    result = cmp_strings(s1->key.domain, s2->key.domain, 256, 0);
    if (result == 0) {
      result = cmp_strings(s1->key.subdomain, s2->key.subdomain, 256, 1);
      if (result == 0) {
	int i;
	for (i = 0; i < 2; i++) {
	  if (s1->key.type[i] < s2->key.type[i]) {
	    result = -1;
	    break;
	  }
	  if (s1->key.type[i] > s2->key.type[i]) {
	    result = 1;
	    break;
	  }
	}
      }
    }
  }
  return result;
}

// Rotate right the subtree rooted at the specified entry
struct cache_entry *avl_rotate_right(struct cache_entry *entry)
{
  struct cache_entry *new_root = entry->avl_node.left;
  struct cache_entry *old_right = new_root->avl_node.right;

  // Perform rotation
  new_root->avl_node.right = entry;
  entry->avl_node.left = old_right;

  // Update heights
  entry->avl_node.height = MAX(avl_height(entry->avl_node.left), avl_height(entry->avl_node.right))+1;
  new_root->avl_node.height = MAX(avl_height(new_root->avl_node.left), avl_height(new_root->avl_node.right))+1;

  return new_root;
}

// Rotate left the subtree rooted at the specified entry
struct cache_entry *avl_rotate_left(struct cache_entry *entry)
{
  struct cache_entry *new_root = entry->avl_node.right;
  struct cache_entry *old_left = new_root->avl_node.left;

  // Perform rotation
  new_root->avl_node.left = entry;
  entry->avl_node.right = old_left;

  //  Update heights
  entry->avl_node.height = MAX(avl_height(entry->avl_node.left), avl_height(entry->avl_node.right))+1;
  new_root->avl_node.height = MAX(avl_height(new_root->avl_node.left), avl_height(new_root->avl_node.right))+1;

  return new_root;
}

// Insert a new cache entry into the AVL tree. Returns root of the modified sub-tree.
struct cache_entry *avl_insert(struct cache_entry *root, struct cache_entry *new_entry)
{
  if (!root) {
    // Setting the height to 1 indicates the entry was inserted
    new_entry->avl_node.height = 1;
    return new_entry;
  }

  if (avl_cmp_nodes(new_entry, root) < 0) {
    root->avl_node.left = avl_insert(root->avl_node.left, new_entry);
  } else if (avl_cmp_nodes(new_entry, root) > 0) {
    root->avl_node.right = avl_insert(root->avl_node.right, new_entry);
  } else {
    // Keys are equal so return pointer to the existing node (new_entry not inserted)
    return root;
  }

  // Update height of this ancestor node if the new entry was added
  if (new_entry->avl_node.height > 0) {
    root->avl_node.height = 1 + MAX(avl_height(root->avl_node.left), avl_height(root->avl_node.right));

    // Get the balance factor of this ancestor so we can check if
    // it has become unbalanced
    int balance = avl_balance(root);
    if (balance > 1) {
      // Left Left Case
      if (avl_cmp_nodes(new_entry, root->avl_node.left) < 0) {
	return avl_rotate_right(root);
      }
      // Left Right Case
      if (avl_cmp_nodes(new_entry, root->avl_node.left) > 0) {
	root->avl_node.left = avl_rotate_left(root->avl_node.left);
	return avl_rotate_right(root);
      }
    }
    if (balance < -1) {
      // Right Left Case
      if (avl_cmp_nodes(new_entry, root->avl_node.right) < 0) {
	root->avl_node.right = avl_rotate_right(root->avl_node.right);
	return avl_rotate_left(root);
      }
      // Right Right Case
      if (avl_cmp_nodes(new_entry, root->avl_node.right) > 0) {
	return avl_rotate_left(root);
      }
    }
  }

  // Return the unchanged entry
  return root;
}

// Given a non-empty binary search tree, return the node with minimum key value
struct cache_entry *avl_find_min_value_node(struct cache_entry *node)
{
  struct cache_entry *min_node = node;
  while (min_node->avl_node.left != NULL)
    min_node = min_node->avl_node.left;
  return min_node;
}

// Balance a sub-tree after a node removal
struct cache_entry *avl_balance_after_node_remove(struct cache_entry *root)
{
  root->avl_node.height = 1 + MAX(avl_height(root->avl_node.left), avl_height(root->avl_node.right));

  // Get the balance factor of this node so we can check if it has become unbalanced
  int balance = avl_balance(root);
  if (balance > 1) {
    // Left Left Case
    if (avl_balance(root->avl_node.left) >= 0) {
      return avl_rotate_right(root);
    }
    // Left Right Case
    if (avl_balance(root->avl_node.left) < 0) {
      root->avl_node.left = avl_rotate_left(root->avl_node.left);
      return avl_rotate_right(root);
    }
  }
  if (balance < -1) {
    // Right Right Case
    if (avl_balance(root->avl_node.right) <= 0) {
      return avl_rotate_left(root);
    }
    // Right Left Case
    if (avl_balance(root->avl_node.right) > 0) {
      root->avl_node.right = avl_rotate_right(root->avl_node.right);
      return avl_rotate_left(root);
    }
  }

  return root;
}

// Remove an entry from the AVL tree. Returns root of the modified sub-tree.
struct cache_entry *avl_remove(struct cache_entry *root, struct cache_entry *search_entry, struct cache_entry **removed_entry) {
  if (!root) {
    return root;
  }

  if (avl_cmp_nodes(search_entry, root) < 0) {
    root->avl_node.left = avl_remove(root->avl_node.left, search_entry, removed_entry);
  } else if (avl_cmp_nodes(search_entry, root) > 0) {
    root->avl_node.right = avl_remove(root->avl_node.right, search_entry, removed_entry);
  } else {
    // This must be the node to be removed
    struct cache_entry *remove_node = root;
    if (remove_node->avl_node.left == NULL) {
      if (remove_node->avl_node.right == NULL) {
        // This is a leaf node with no children
	*removed_entry = remove_node;
	return NULL;
      }
      root = remove_node->avl_node.right;
      remove_node->avl_node.right = NULL;
    } else if (remove_node->avl_node.right == NULL) {
      root = remove_node->avl_node.left;
      remove_node->avl_node.left = NULL;
    } else {
      // The node has two children. Move the in-order successor to root (smallest in the right sub-tree)
      struct cache_entry *successor = avl_find_min_value_node(root->avl_node.right);
      successor->avl_node.right = avl_remove(root->avl_node.right, successor, removed_entry);
      successor->avl_node.left = root->avl_node.left;
      root->avl_node.right = NULL;
      root->avl_node.left = NULL;
      root = successor;
    }
    *removed_entry = remove_node;
  }

  // Re-balance this root node if an entry was removed
  if (*removed_entry) {
    root = avl_balance_after_node_remove(root);
  }

  return root;
}

// Remove an entry from the AVL tree that matches the filter. Returns root of the modified sub-tree.
struct cache_entry *avl_remove_with_filter(struct cache_entry *root, struct search_filter *filter, struct cache_entry **removed_entry) {
  if (!root) {
    return root;
  }

  int result = 0;
  if (filter->tld) {
    result = cmp_strings(filter->tld, root->key.tld, 256, 0);
  }
  if (result == 0 && filter->domain) {
    result = cmp_strings(filter->domain, root->key.domain, 256, 0);
  }
  if (result < 0) {
    // Nothing to the right so just go left
    root->avl_node.left = avl_remove_with_filter(root->avl_node.left, filter, removed_entry);
  } else if (result > 0) {
    // Nothing to the left so just go right
    root->avl_node.right = avl_remove_with_filter(root->avl_node.right, filter, removed_entry);
  } else if (!filter->type || memcmp(filter->type, root->key.type, 2) == 0) {
    // This node can be removed
    struct cache_entry *remove_node = root;
    if (remove_node->avl_node.left == NULL) {
      if (remove_node->avl_node.right == NULL) {
	// This is a leaf node with no children
	*removed_entry = remove_node;
	return NULL;
      }
      root = remove_node->avl_node.right;
      remove_node->avl_node.right = NULL;
    } else if (remove_node->avl_node.right == NULL) {
      root = remove_node->avl_node.left;
      remove_node->avl_node.left = NULL;
    } else {
      // The node has two children. Move the in-order successor to root (smallest in the right sub-tree)
      struct cache_entry *successor = avl_find_min_value_node(root->avl_node.right);
      successor->avl_node.right = avl_remove(root->avl_node.right, successor, removed_entry);
      successor->avl_node.left = root->avl_node.left;
      root->avl_node.right = NULL;
      root->avl_node.left = NULL;
      root = successor;
    }
    *removed_entry = remove_node;
  }

  // Re-balance this root node if an entry was removed
  if (*removed_entry) {
    root = avl_balance_after_node_remove(root);
  }

  return root;
}

// Find an entry in the AVL tree. Returns NULL if not found.
struct cache_entry *avl_find(struct cache_entry *root, struct cache_entry *search_entry)
{
  if (root) {
    if (avl_cmp_nodes(search_entry, root) < 0) {
      return avl_find(root->avl_node.left, search_entry);
    }
    if (avl_cmp_nodes(search_entry, root) > 0) {
      return avl_find(root->avl_node.right, search_entry);
    }
  }
  return root;
}

// Walk the AVL tree in sorted order and call the callback function for each node
void avl_treewalk(struct cache_entry *root, struct cache_treewalk_args *args)
{
  if (root) {
    // If using a filter, only callback those nodes that match
    int result = 0;
    if (args->filter.tld) {
      result = cmp_strings(args->filter.tld, root->key.tld, 256, 0);
    }
    if (result == 0 && args->filter.domain) {
      result = cmp_strings(args->filter.domain, root->key.domain, 256, 0);
    }
    if (result < 0) {
      // Nothing to the right so just go left
      avl_treewalk(root->avl_node.left, args);
      return;
    }
    if (result > 0) {
      // Nothing to the left so just go right
      avl_treewalk(root->avl_node.right, args);
      return;
    }

    avl_treewalk(root->avl_node.left, args);
    if (!args->filter.type || memcmp(args->filter.type, root->key.type, 2) == 0) {
      args->callback(&root->key, root->data, root->data_len);
    }
    avl_treewalk(root->avl_node.right, args);
  }
}

// Check cache for compliance
void cache_avl_check_compliance(void)
{
  struct cache_entry *entry;
  struct cache_entry *removed_entry;

  // Remove entries which have expired.
  // The oldest cache entries are at the beginning of the age list.
  while (age_root.head) {
    entry = container_of(age_root.head, struct cache_entry, age_node);
    struct tai now;
    tai_now(&now);
    if (!tai_less(&entry->expire, &now)) {
      break;
    }
    removed_entry = NULL;
    avl_root = avl_remove(avl_root, entry, &removed_entry);
    delete_cache_entry(entry);
  }

  // Remove least used entries to bring cache size into compliance
  // The least used cache entries are at the beginning of the usage list.
  while (usage_root.head && cache_size > cache_size_max) {
    entry = container_of(usage_root.head, struct cache_entry, usage_node);
    removed_entry = NULL;
    avl_root = avl_remove(avl_root, entry, &removed_entry);
    delete_cache_entry(entry);
  }
}

// The domain comes in formatted like this: [len[1]dom[len][len[1]dom[len]...]0[1]]
void cache_avl_set(const char type[2], const char *domain, const char *data, unsigned int datalen, uint32 ttl)
{
  if (!ttl) return;
  if (ttl > 604800) ttl = 604800;

  // Create a new cache entry
  struct cache_entry *new_entry = create_cache_entry(type, domain, data, datalen, ttl);
  if (!new_entry) {
    return;
  }

  // Insert the new entry into the AVL tree
  avl_root = avl_insert(avl_root, new_entry);
  if (new_entry->avl_node.height == 0) {
    // The new entry wasn't inserted because an entry with the same key already exists
    delete_cache_entry(new_entry);
    return;
  }

  // Add to the age and usage lists (newest will be at end of list)
  dll_push_back(&age_root, &new_entry->age_node);
  dll_push_back(&usage_root, &new_entry->usage_node);
  if ((cache_size += new_entry->entry_len) > cache_size_max) {
    cache_avl_check_compliance();
  }
}

// Clear/delete an entry in the cache
void cache_avl_clear(const char type[2], const char *domain)
{
  struct cache_entry *search_entry = create_cache_entry(type, domain, NULL, 0, 0);
  if (!search_entry) {
    return;
  }
  struct cache_entry *removed_entry = NULL;
  avl_root = avl_remove(avl_root, search_entry, &removed_entry);
  if (removed_entry) {
    delete_cache_entry(removed_entry);
  }
  delete_cache_entry(search_entry);
}

// Clear/delete entries in the cache that match the specified filter
void cache_avl_clear_with_filter(struct search_filter *filter)
{
  while (1) {
    struct cache_entry *removed_entry = NULL;
    avl_root = avl_remove_with_filter(avl_root, filter, &removed_entry);
    if (!removed_entry) {
      break;
    }
    delete_cache_entry(removed_entry);
  }
}

// Lookup a cache entry
char *cache_avl_get(const char type[2], const char *domain, unsigned int *datalen, uint32 *ttl)
{
  struct cache_entry *search_entry = create_cache_entry(type, domain, NULL, 0, 0);
  if (search_entry) {
    struct cache_entry *found_entry = avl_find(avl_root, search_entry);
    delete_cache_entry(search_entry);
    if (found_entry) {
      struct tai now;
      tai_now(&now);
      if (!tai_less(&found_entry->expire, &now)) {
	struct tai expire;
	tai_sub(&expire, &found_entry->expire, &now);
	double d = tai_approx(&expire);
	if (d > 604800) d = 604800;
	*ttl = d;
	*datalen = found_entry->data_len;
	dll_move_back(&usage_root, &found_entry->usage_node);
	return found_entry->data;
      } else {
        // Age out this cache entry
	cache_avl_check_compliance();
      }
    }
  }
  return NULL;
}

// Walk the AVL tree and call the callback function for each node
void cache_avl_treewalk(struct cache_treewalk_args *args)
{
  avl_treewalk(avl_root, args);
}

// Get the number of entries in the cache
int cache_avl_get_num_entries(void)
{
  return age_root.node_count;
}

// Set the max cache size limit
int cache_avl_init(unsigned int cachesize)
{
  cache_size_max = cachesize;
  cache_avl_check_compliance();
}
