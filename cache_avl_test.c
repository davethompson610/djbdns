/*
 *  UT for AVL tree based caching mechanism.
 */

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include "cache_avl.h"
#include "dns.h"

int error_count = 0;
int test_count = 0;

#define EXPECT_INT_EQ(f, l, a, b) {                    \
  if (a != b) {                                        \
    printf("%s(%d): EXPECTED %d == %d\n", f, l, a, b); \
    error_count++;                                     \
  }                                                    \
}
#define EXPECT_UINT_EQ(f, l, a, b) {                   \
  if (a != b) {                                        \
    printf("%s(%d): EXPECTED %lu == %lu\n", f, l, (unsigned long)a, (unsigned long)b); \
    error_count++;                                     \
  }                                                    \
}
#define EXPECT_STR_EQ(f, l, a, b) {                    \
  if (strcmp(a,b) != 0) {                              \
    printf("%s(%d): EXPECTED %s == %s\n", f, l, a, b); \
    error_count++;                                     \
  }                                                    \
}
#define EXPECT_TRUE(f, l, v) {                         \
  if (!v) {                                            \
    printf("%s(%d): EXPECTED TRUE\n", f, l);           \
    error_count++;                                     \
  }                                                    \
}
#define EXPECT_FALSE(f, l, v) {                        \
  if (v) {                                             \
    printf("%s(%d): EXPECTED FALSE\n", f, l);          \
    error_count++;                                     \
  }                                                    \
}

void test_init_cache(unsigned int cachesize)
{
  // Setting to zero first will clear the cache
  cache_avl_init(0);
  cache_avl_init(cachesize);
}

// Format a domain name
char *fdom(const char *dom)
{
  static char urlf[256];
  char *last = &urlf[0];
  char len = 0;
  int i = 0;
  while (1) {
    char ch = dom[i++];
    urlf[i] = ch;
    if (ch == '.' || ch == '\0') {
      *last = len;
      len = 0;
      last = &urlf[i];
      if (ch == '\0') {
        break;
      }
    } else {
      len++;
    }
  }
  return urlf;
}

// Use this to print the avl tree
void test_treewalk_print(struct cache_entry_key *key, char *data, unsigned int datalen)
{
  printf("%02x%02x ", key->type[0], key->type[1]);

  // Print the subdomain
  const char *cp = key->subdomain;
  const char *dot = "";
  while (*cp) {
    char name[256];
    memcpy(name, &cp[1], *cp);
    name[*cp] = '\0';
    printf("%s%s", dot, name);
    dot = ".";
    cp += (unsigned int) *cp + 1;
  }

  if (key->domain[0]) {
    printf("%s%s", dot, key->domain);
    dot = ".";
  }
  if (key->tld[0]) {
    printf("%s%s", dot, key->tld);
  }
  printf("\n");
  test_count++;
}

void test_basic(void)
{
  test_init_cache(2048);

  struct test_data {
    char dom[256];
    char data[256];
  } data[] = {
      {"www.google.com", "The quick brown fox"},
      {"www.cisco.com", "jumped over the sleeping"},
      {"www.facebook.com", "lazy dogs"},
      {"topas.com", "I love tacos"},
      {"irs.gov", "I dislike taxes"},
      {"whitehouse.gov", "It's gonna be great, trust me"},
      {"login.eurid.eu", "+ My login"},
      {"www.eurid.eu", "+ Register a domain name"},
      {"eurid.eu", "+ Or use this for short"},
      {"", ""}
  };

  // Add some cache entries
  int count, expected_count = 0;
  int expected_filter_count = 0;
  for (count=0; data[count].dom[0]; count++) {
    cache_avl_set(DNS_C_IN, fdom(data[count].dom), data[count].data, strlen(data[count].data)+1, 1000);
    expected_count++;
    if (data[count].data[0] == '+') {
      expected_filter_count++;
    }
    EXPECT_INT_EQ(__FILE__, __LINE__, expected_count, cache_avl_get_num_entries());
  }

  // Verify we can find them
  for (count=0; data[count].dom[0]; count++) {
    unsigned int datalen = 0;
    uint32 ttl = 0;
    char *found_data = cache_avl_get(DNS_C_IN, fdom(data[count].dom), &datalen, &ttl);
    EXPECT_TRUE(__FILE__, __LINE__, found_data);
    if (found_data) {
      EXPECT_UINT_EQ(__FILE__, __LINE__, strlen(data[count].data)+1, datalen);
      EXPECT_STR_EQ(__FILE__, __LINE__, data[count].data, found_data);
    }
  }

  // Print all the nodes
  test_count = 0;
  struct cache_treewalk_args treewalk_args;
  memset(&treewalk_args, 0, sizeof(struct cache_treewalk_args));
  treewalk_args.callback = test_treewalk_print;
  printf("\nAll nodes:\n");
  cache_avl_treewalk(&treewalk_args);
  EXPECT_INT_EQ(__FILE__, __LINE__, expected_count, test_count);

  // Print only the nodes that match the domain "eurid.eu"
  test_count = 0;
  treewalk_args.filter.tld = "eu";
  treewalk_args.filter.domain = "eurid";
  printf("\nOnly nodes with domain %s.%s:\n", treewalk_args.filter.domain, treewalk_args.filter.tld);
  cache_avl_treewalk(&treewalk_args);
  EXPECT_INT_EQ(__FILE__, __LINE__, expected_filter_count, test_count);

  // Remove using filter
  cache_avl_clear_with_filter(&treewalk_args.filter);
  expected_count -= expected_filter_count;
  EXPECT_INT_EQ(__FILE__, __LINE__, expected_count, cache_avl_get_num_entries());

  // Remove them
  for (count=0; data[count].dom[0]; count++) {
    if (data[count].data[0] != '+') {
      cache_avl_clear(DNS_C_IN, fdom(data[count].dom));
      expected_count--;
      EXPECT_INT_EQ(__FILE__, __LINE__, expected_count, cache_avl_get_num_entries());
    }

    unsigned int datalen = 0;
    uint32 ttl = 0;
    char *found_data = cache_avl_get(DNS_C_IN, fdom(data[count].dom), &datalen, &ttl);
    EXPECT_FALSE(__FILE__, __LINE__, found_data);
  }
}

int main(int argc,char **argv)
{
  test_basic();
  if (error_count) {
    printf("\nTests completed with %d errors\n", error_count);
  } else {
    printf("\nTests completed successfully\n");
  }
  return 0;
}