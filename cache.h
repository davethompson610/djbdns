#ifndef CACHE_H
#define CACHE_H

#include "uint32.h"
#include "uint64.h"

extern uint64 cache_motion;
extern int cache_init(unsigned int);
extern void cache_set(const char *,unsigned int,const char *,unsigned int,uint32);
extern void cache_clear(const char *,unsigned int);
extern char *cache_get(const char *,unsigned int,unsigned int *,uint32 *);

#endif
