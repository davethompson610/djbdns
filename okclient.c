#include <dirent.h>
#include <pthread.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "direntry.h"
#include "okclient.h"

struct ipaccess {
  char ip[4];
  char mask[4];
  struct ipaccess *next;
};

static pthread_t tid;
static int abort_thread = 0;
static struct ipaccess *permit_list = NULL;
pthread_rwlock_t rwlock;

// Get the IPv4 and mask from the string.
// Example: "10"        = ipv4=10.0.0.0,mask=255.0.0.0
//          "172.17"    = ipv4=172.17.0.0,mask=255.255.0.0
//          "127.0.0.1" = ipv4=127.0.0.1,mask=255.255.255.255
// Returns zero if a bad address found.
int get_ip_access(const char *ip_str, struct ipaccess *ipa) {
  char ch = 1;
  int i, j=0;
  for (i=0; i < 4; i++) {
    int ipbyte = 0;
    int maskbyte = 0;
    while (ch != '\0') {
      ch = ip_str[j++];
      if (ch == '\0') {
	break;
      }
      if (ch == '.') {
        // Shouldn't have a mask byte of zero here
        if (maskbyte == 0) {
          return 0;
        }
	break;
      }
      if (ch < '0' || ch > '9') {
        // Non-decimal number
        return 0;
      }
      ipbyte = ipbyte * 10 + ch - '0';
      if (ipbyte > 255) {
        // Decimal number too big
        return 0;
      }
      maskbyte = 255;
    }
    ipa->ip[i] = (char)ipbyte;
    ipa->mask[i] = (char)maskbyte;
  }
  return 1;
}

// Monitor the client access list for changes
void *monitor_client_access(void *arg)
{
  time_t mtimeprev = 0;

  // This is essentially loop forever
  while (!abort_thread) {
    struct stat st;
    // If the ip directory hasn't changed, nothing to do
    if (stat("ip", &st) == 0 && st.st_mtime != mtimeprev) {
      struct ipaccess *head = NULL;
      struct ipaccess *tail = NULL;

      // Go through the ip directory and re-create our permit list.
      // Addresses found here are permitted access.
      DIR *dir = opendir("ip");
      if (dir != NULL) {
	direntry *d;
	while ((d = readdir(dir)) != NULL) {
	  if (d->d_name[0] != '.') {
	    // Parse into an IPv4 address with mask
	    struct ipaccess ipa;
	    if (get_ip_access(d->d_name, &ipa)) {
	      struct ipaccess *node = malloc(sizeof(ipa));
	      if (node != NULL) {
		memcpy(node->ip, ipa.ip, sizeof(node->ip));
		memcpy(node->mask, ipa.mask, sizeof(node->mask));
		node->next = NULL;
		if (tail != NULL) {
		  tail->next = node;
		} else {
		  head = node;
		}
		tail = node;
	      }
	    }
	  }
	}
	closedir(dir);
      }

      // Replace the access list
      pthread_rwlock_wrlock(&rwlock);
      struct ipaccess *oldhead = permit_list;
      permit_list = head;
      pthread_rwlock_unlock(&rwlock);

      // Free the old list
      while (oldhead != NULL) {
	struct ipaccess *node = oldhead;
	oldhead = node->next;
	free(node);
      }

      mtimeprev = st.st_mtime;
    }

    sleep(10);
  }
}

// Initialize the client access list.
// Returns zero on failure
int okclient_init(void)
{
  pthread_rwlock_init(&rwlock, NULL);
  return pthread_create(&tid, NULL, &monitor_client_access, NULL) == 0 ? 1 : 0;
}

// Check if ok to proceed with query from the client with the specified ip.
// Returns one if ok, zero if not.
int okclient(char ip[4])
{
  struct ipaccess *node;
  int ok = 0;
  pthread_rwlock_rdlock(&rwlock);
  for (node = permit_list; node != NULL; node = node->next) {
    int found = 1;
    int i;
    for (i=0; i < 4; i++) {
      if ((ip[i] & node->mask[i]) != node->ip[i]) {
        found = 0;
        break;
      }
    }
    if (found) {
      ok = 1;
      break;
    }
  }
  pthread_rwlock_unlock(&rwlock);
  return ok;
}
