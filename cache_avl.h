#ifndef CACHE_AVL_H
#define CACHE_AVL_H

#include "uint32.h"

// This is the key for a cache entry.
// The tld and domain strings are regular nul terminated strings.
// The subdomain is in the len[1]dom[len][0] format.
struct cache_entry_key {
  char *tld;
  char *domain;
  char *subdomain;
  char  type[2];
};

// Callback used by the AVL treewalk function
typedef void (*cache_avl_treewalk_callback_func)(struct cache_entry_key *, char *, unsigned int);

// Search filters
struct search_filter {
  const char *type;
  const char *tld;
  const char *domain;
};

// Arguments to the AVL treewalk function
struct cache_treewalk_args {
  struct search_filter             filter;
  cache_avl_treewalk_callback_func callback;
};

extern int cache_avl_init(unsigned int);
extern void cache_avl_set(const char *, const char *, const char *, unsigned int,uint32);
extern void cache_avl_clear(const char *, const char *);
extern void cache_avl_clear_with_filter(struct search_filter *);
extern char *cache_avl_get(const char *, const char *, unsigned int *, uint32 *);
extern void cache_avl_treewalk(struct cache_treewalk_args *);
extern int cache_avl_get_num_entries(void);

#endif
